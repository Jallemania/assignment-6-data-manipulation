# Assigment 6 Data Manipulation 

Data acess with SQL Client
Write SQL scripts and build a console application in C#. 
The assigment is completed in groups of 2 and the main purpuse is to complete it while Pair Programming. 

## Description

Assigment 6 is made of two parts. 
The first part is to create SQL scripts and create a database named SuperheroDb and various scripts to set up tables, add relationships and populate tables with data. 
The second part of the assigment, we were assigned to manipulate SQL Server data in Visual Studio using a libary called SQL Client. 
For this part we were given a database named Chinook to work with. We were to create a C# console application and create a repository to interact with the database. 


## Getting Started

To run the project it's neccessery to have, a IDE as Visual Studio and a SQL Server Management Studio. 
Visual Studio to run the console application and the SQL Server Management Studio to run the scripts that are provided in a folder named SqlQuerys. 



### Dependencies

* Visual Studio
* Microsoft SQL Server Managment Studio 18
* Windows 10
* .NET 6 
    - Microsoft.Data.SqlClient NuGet-Package
* C# 
* SQL
* Chinook Database


### Installing

* How/where to download your program
    - git clone https://gitlab.com/Jallemania/assignment-6-data-manipulation.git 

### Executing program

* How to run the scripts from the SqlQuerys folder
    - Description of how to login in and connect is provided by : [Quickstart: Connect and query a SQL Server instance using SQL Server Management Studio (SSMS)](https://docs.microsoft.com/en-us/sql/ssms/quickstarts/ssms-connect-query-sql-server?view=sql-server-ver16)
    - Then just follow the number structure of the folder:
        - Ex: 01_dbCreate and press execute. Then will a new database be created.
        - Then proceed to execute all the following querys in order. 

* How to run the console application with Visual Studio 
    - Clone repository and open with Visual Studio.
    - Install NuGet Package: Microsoft.Data.SqlClient.
    - In the ConnectionStringHelper.cs file, change the DataSource too your own server name.


## Authors

* Marcus Jarlevid [@Jallemania]  
    - marcus.jarlevid@se.experis.com
* Elmin Nurkic [@gfunkmaster]
    - elmin.nurkic@se.experis.com

## Version History

* 0.1
    * Initial Release

