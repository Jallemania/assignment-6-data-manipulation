﻿using Data_Manipulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess.Interfaces
{
    public interface ICustomerCountryRepository
    {
        /// <summary>
        /// Gets a list of countries with the number of customers of each country ordered in descending order.
        /// </summary>
        /// <returns>A list of CustomerCountry</returns>
        public List<CustomerCountry> GetCustomersByCountry();
    }
}
