﻿using Data_Manipulation.DataAccess.Interfaces;
using Data_Manipulation.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess
{
    internal class CustomerCountryRepository : ICustomerCountryRepository
    {

        public List<CustomerCountry> GetCustomersByCountry()
        {
            List<CustomerCountry> customerCountries = new();
            string sql = "SELECT Country AS 'Countries', COUNT(CustomerID) AS 'Customers' FROM Customer GROUP BY Country ORDER BY COUNT(CustomerID) DESC";

            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    //Command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //Handle
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new();

                                if (!reader.IsDBNull(0)) customerCountry.Country = reader.GetString(0);
                                if (!reader.IsDBNull(1)) customerCountry.Customers = reader.GetInt32(1);

                                customerCountries.Add(customerCountry);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return customerCountries;
        }
    }
}
