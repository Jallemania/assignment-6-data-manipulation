﻿using Data_Manipulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Gets all customers and returns them in a List
        /// </summary>
        /// <returns>A list of Customers</returns>
        public List<Customer> GetAll();

        /// <summary>
        /// Gets a specified subset of customers based on the parameters.
        /// </summary>
        /// <param name="offset">Specifies from which index the subset should start</param>
        /// <param name="limit">Specifies how many customers to get</param>
        /// <returns>A list of Customers</returns>
        public List<Customer> GetPage(int offset, int limit);

        /// <summary>
        /// Get Customer by Id.
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns>a Customer</returns>
        public Customer GetCustomer(int id);

        /// <summary>
        /// Gets Customer by name.
        /// </summary>
        /// <param name="name">A string representing the customers name</param>
        /// <returns>a Customer</returns>
        public Customer GetCustomer(string name);

        /// <summary>
        /// Add a new customer to the database.
        /// </summary>
        /// <param name="customer">The new customer</param>
        /// <returns>The customer that was just created</returns>
        public bool Add(Customer customer);

        /// <summary>
        /// Gets an existing customer and updates it.
        /// </summary>
        /// <param name="customer">The new instance of the customer with updated properties</param>
        /// <returns></returns>
        public bool Update(Customer customer);

        /// <summary>
        /// Removes a customer from the database.
        /// </summary>
        /// <param name="id">The id of the customer to be removed</param>
        /// <returns>A bool based on if the operation was successful</returns>
        public bool Delete(int id);
    }
}
