﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.Models
{
    public class Customer
    {
        public int CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public int SupportRepId { get; set; }

        public Customer()
        {
            CustomerID = 0;
            FirstName = "Not set";
            LastName = "Not set";
            Company = "Not set";
            Address = "Not set";
            City = "Not set";
            State = "Not set";
            Country = "Not set";
            PostalCode = "Not set";
            Phone = "Not set";
            Fax = "Not set";
            Email = "Not set";
            SupportRepId = 0;
        }

        public Customer(int customerID, string firstName, string lastName, string company, string address, string city, string state, string country, string postalCode, string phone, string fax, string email, int supportRepId)
        {
            CustomerID = customerID;
            FirstName = firstName;
            LastName = lastName;
            Company = company;
            Address = address;
            City = city;
            State = state;
            Country = country;
            PostalCode = postalCode;
            Phone = phone;
            Fax = fax;
            Email = email;
            SupportRepId = supportRepId;
        }
    }
}
