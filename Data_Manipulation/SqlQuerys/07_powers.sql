USE SuperheroDb;


INSERT INTO Power(Name, Description)
VALUES 
('Cloaking', 'Being able to hide from some or any type of optical sight'),
('Data Manipulation', 'The ability to manipulate computer data'),
('Flight', 'Allowing the user to lift off the ground and fly through the air'),
('Mind Blast', 'Ability to overload anothers mind causing pain, memory loss')
 
INSERT INTO Power_Hero_Link(HeroID, PowerID)
VALUES 
(1,1), --RickAttack + Cloaking
(1,2), -- RickAttack + Mind Blast
(2,2), -- Homer + Data Manipulation
(3,3), -- Batman + Flight
(3,1) -- Batman + Cloaking