﻿using Data_Manipulation.DataAccess.Interfaces;
using Data_Manipulation.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess
{
    public class CustomerSpenderRepository : ICustomerSpenderRepository
    {
        public List<CustomerSpender> GetHighestSpenders()
        {
            List<CustomerSpender> customerSpenders = new();
            string sql = "SELECT Invoice.CustomerID, SUM(Total) AS 'Total Spent', CONCAT(Customer.FirstName, ' ', Customer.LastName) AS 'Full Name' FROM Invoice LEFT JOIN Customer ON Invoice.CustomerId = Customer.CustomerId GROUP BY Invoice.CustomerId, Customer.FirstName, Customer.LastName ORDER BY SUM(Total) DESC";

            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    //Command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //Handle
                            while (reader.Read())
                            {
                                CustomerSpender customerSpender = new()
                                {
                                    CustomerID = reader.GetInt32(0),
                                    InvoiceTotal = reader.GetDecimal(1),
                                    Name = reader.GetString(2),
                                };

                                customerSpenders.Add(customerSpender);  
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return customerSpenders;
        }

        
    }
}
