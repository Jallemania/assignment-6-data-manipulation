﻿using Data_Manipulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess.Interfaces
{
    public interface ICustomerGenreRepository
    {
        /// <summary>
        /// Takes in an Customer Id and then gets the favourite genre of that Customer based on purchases.
        /// Returns two ganres if customer has equal amount of purchases in each genre.
        /// </summary>
        /// <param name="id">Id of the Customer</param>
        /// <returns>CustomGenre</returns>
        public CustomerGenre GetFavoriteGenre(int id);
    }
}
