﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.Models
{
    public class CustomerGenre
    {
        public Customer Customer { get; set; }
        public List<string> FavoriteGenre { get; set; }
        public int PurchasesGenre { get; set; }

        public CustomerGenre()
        {
            FavoriteGenre = new();
            PurchasesGenre = 0;
            Customer = new();
        }

    }
}
