USE SuperheroDb

CREATE TABLE Power_Hero_Link (
	HeroID int not null, 
	PowerID int not null, 
	CONSTRAINT PK_PowerHeroLink PRIMARY KEY (HeroID, PowerID), 
	CONSTRAINT FK_PowerHeroLink_Hero FOREIGN KEY (HeroID) REFERENCES Superhero(HeroID), 
	CONSTRAINT FK_PowerHeroLink_Power FOREIGN KEY (PowerID) REFERENCES Power(PowerID), 
)


