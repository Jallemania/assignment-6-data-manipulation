USE SuperheroDb;


INSERT INTO Superhero( Name, Alias, Origin)
VALUES 
( 'RickAttack', 'Rickard Lionheart', 'A Superhero of Fullstack knowledge'),
( 'Homer', 'Homer Simpson', 'A Superhero of Accidents'),
( 'Batman', 'Bruce Wayne', 'A Superhero of Vengence')
