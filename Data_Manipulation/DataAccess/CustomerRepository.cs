﻿using Data_Manipulation.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess
{
    public class CustomerRepository : ICustomerRepository
    {
        public bool Add(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " + 
                "VALUES(@FirstName, @LastName,@Country,@PostalCode,@Phone,@Email)";

            try
            {
                using(SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw ex;
            }

            return success; 
        }

        public bool Update(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName," +
                " Company = @Company, Address = @Address, City = @City, State = @State, " +
                "Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Fax = @Fax, Email = @Email" +
                $" WHERE CustomerID = {customer.CustomerID}";

            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Company", customer.Company);
                        cmd.Parameters.AddWithValue("@Address", customer.Address);
                        cmd.Parameters.AddWithValue("@City", customer.City);
                        cmd.Parameters.AddWithValue("@State", customer.State);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Fax", customer.Fax);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);

                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return success;
        }
        
        public bool Delete(int id)
        {
            bool success = false;
            string sql = $"DELETE FROM Customer WHERE CustomerID = {id}";

            try
            {
                using (SqlConnection conn = new(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new(sql, conn))
                    {
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return success;
        }

        public List<Customer> GetAll()
        {
            List<Customer> customers = new List<Customer>();
            string sql = "SELECT CustomerID, Country, FirstName, LastName, Phone, Email, PostalCode FROM Customer";
            
            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    //Command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using(SqlDataReader reader = command.ExecuteReader())
                        {
                            //Handle
                            while (reader.Read())
                            {
                                Customer customer = new Customer();

                                customer.CustomerID = reader.GetInt32(0);
                                if (!reader.IsDBNull(1)) customer.Country = reader.GetString(1);
                                if (!reader.IsDBNull(2)) customer.FirstName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.LastName = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.Phone = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Email = reader.GetString(5);
                                if (!reader.IsDBNull(6)) customer.PostalCode = reader.GetString(6);

                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return customers;   
        }

        public Customer GetCustomer(int id)
        {
            Customer customer = new();
            string sql = $"SELECT CustomerID, Country, FirstName, LastName, Phone, Email, PostalCode FROM Customer WHERE CustomerID = {id}";

            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    //Command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //Handle
                            while (reader.Read())
                            {
                                customer.CustomerID = reader.GetInt32(0);
                                if (!reader.IsDBNull(1)) customer.Country = reader.GetString(1);
                                if (!reader.IsDBNull(2)) customer.FirstName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.LastName = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.Phone = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Email = reader.GetString(5);
                                if (!reader.IsDBNull(6)) customer.PostalCode = reader.GetString(6);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return customer;
        }
        
        public Customer GetCustomer(string name)
        {
            Customer customer = new();
            string sql = $"SELECT CustomerID, Country, FirstName, LastName, Phone, Email, PostalCode FROM Customer WHERE UPPER (FirstName) LIKE UPPER ('{name}%')";

            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    //Command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //Handle
                            while (reader.Read())
                            {
                                customer.CustomerID = reader.GetInt32(0);
                                if (!reader.IsDBNull(1)) customer.Country = reader.GetString(1);
                                if (!reader.IsDBNull(2)) customer.FirstName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.LastName = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.Phone = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Email = reader.GetString(5);
                                if (!reader.IsDBNull(6)) customer.PostalCode = reader.GetString(6);

                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return customer;
        }

        public List<Customer> GetPage(int offset, int limit)
        {
            List<Customer> customers = new List<Customer>();
            string sql = $"SELECT CustomerID, Country, FirstName, LastName, Phone, Email, PostalCode FROM Customer ORDER BY CustomerID OFFSET {offset} ROWS FETCH NEXT {limit} ROWS ONLY";

            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    //Command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //Handle
                            while (reader.Read())
                            {
                                Customer customer = new Customer();

                                customer.CustomerID = reader.GetInt32(0);
                                if (!reader.IsDBNull(1)) customer.Country = reader.GetString(1);
                                if (!reader.IsDBNull(2)) customer.FirstName = reader.GetString(2);
                                if (!reader.IsDBNull(3)) customer.LastName = reader.GetString(3);
                                if (!reader.IsDBNull(4)) customer.Phone = reader.GetString(4);
                                if (!reader.IsDBNull(5)) customer.Email = reader.GetString(5);
                                if (!reader.IsDBNull(6)) customer.PostalCode = reader.GetString(6);

                                customers.Add(customer);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return customers;
        }

    }
}
