﻿using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess
{
    public class ConnectionStringHelper
    {

        /// <summary>
        /// Configures SqlConnectionStringBuilder. Sets DataSource, InitialCatalog, and IntegratedSecurity.
        /// </summary>
        /// <returns>Returns the connection string</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new();
            connectionStringBuilder.DataSource = "N-SE-01-3674"; // Your server name here
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.Encrypt = false; //set this to false because had problem with Certificate verification

            return connectionStringBuilder.ConnectionString;
        }
    }
}
