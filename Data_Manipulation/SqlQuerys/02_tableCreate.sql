USE SuperheroDb

CREATE TABLE Superhero(
HeroID int PRIMARY KEY IDENTITY(1,1),
Name nvarchar(50) null, 
Alias nvarchar(25) null,
Origin nvarchar(50) null, 

)

CREATE TABLE Assistant (
AssistantID int Primary Key identity(1,1),
Name nvarchar(50) null,
)

CREATE TABLE Power(
PowerID int PRIMARY KEY IDENTITY(1,1),
Name nvarchar(50) null, 
Description nvarchar(150) null

)
