﻿using Data_Manipulation.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess.Interfaces
{
    public interface ICustomerSpenderRepository
    {
        /// <summary>
        /// Gets the total amount of purchases and returns a list of the highest spending customers in descending order.
        /// </summary>
        /// <returns>A list of CustomeSpender</returns>
        public List<CustomerSpender> GetHighestSpenders();

    }
}
