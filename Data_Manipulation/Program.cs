﻿using Data_Manipulation.DataAccess;
using Data_Manipulation.DataAccess.Interfaces;
using Data_Manipulation.Models;


ICustomerRepository repo = new CustomerRepository();
ICustomerCountryRepository cRepo =  new CustomerCountryRepository();
ICustomerSpenderRepository sRepo = new CustomerSpenderRepository();
ICustomerGenreRepository gRepo = new CustomerGenreRepository();

string input = "";

do
{
    Console.Clear();
    Console.WriteLine("\n----- ┼┼ ██▄ ███ ███ ███ ██▄ ███ ███ ███ ┼┼ █▄┼▄█ ███ █┼┼█ ███ ███ █┼█ █┼┼ ███ ███ ███ ███ ┼┼-----\n----- ┼┼ █┼█ █▄█ ┼█┼ █▄█ █▄█ █▄█ █▄▄ █▄┼ ┼┼ █┼█┼█ █▄█ ██▄█ ┼█┼ █▄█ █┼█ █┼┼ █▄█ ┼█┼ █┼█ █▄┼ ┼┼-----\n----- ┼┼ ███ █┼█ ┼█┼ █┼█ █▄█ █┼█ ▄▄█ █▄▄ ┼┼ █┼┼┼█ █┼█ █┼██ ▄█▄ █┼┼ ███ █▄█ █┼█ ┼█┼ █▄█ █┼█ ┼┼-----\n");
    Console.WriteLine("Please input the preferred action, or press 'q' to quit: \n\n 1 = Get all customers \n 2 = Get customer by Id \n 3 = Get customer by name \n 4 = Get list of customers \n 5 = Get number of customers in each country \n 6 = Get highest spenders \n 7 = Get a customers favourite genre \n 8 = Add new customer \n 9 = Edit existing customer \n 0 = Delete customer\n");

    switch (input)
    {
        case "1":
            TestSelectAll(repo);
            break;
        case "2":
            TestSelectById(repo);
            break;
        case "3":
            TestSelectByName(repo);
            break;
        case "4":
            TestSelectPage(repo);
            break;
        case "5":
            TestSelectCountries(cRepo);
            break;
        case "6":
            TestSelectHighestSpenders(sRepo);
            break;
        case "7":
            TestSelectFavouriteGenre(gRepo);
            break;
        case "8":
            TestInsert(repo);
            break;
        case "9":
            TestUpdate(repo);
            break;
        case "0":
            TestDelete(repo);
            break;
        default:
            break;
    }

    input = Console.ReadLine();
    
} while (input != "q");


//Read All
static void TestSelectAll(ICustomerRepository repository)
{
    Console.Clear();
    Console.WriteLine("Test Select All Customers: \n");
    PrintCustomers(repository.GetAll());
}

//Read Single by id
static void TestSelectById(ICustomerRepository repository)
{
    Console.Clear();
    Console.WriteLine("Please enter customer id");

    int.TryParse(Console.ReadLine(), out int id);

    Console.Clear();
    Printcustomer(repository.GetCustomer(id));
}

//Read Single by name
static void TestSelectByName(ICustomerRepository repository)
{
    Console.Clear();
    Console.WriteLine("Please enter customer name:");

    string name = Console.ReadLine();
    Console.Clear();

    Printcustomer(repository.GetCustomer(name));
}

//Read page by limit and offset
static void TestSelectPage(ICustomerRepository repository)
{
    try
    {
        Console.Clear();
        Console.WriteLine("Please enter how many customers you want to fetch:");

        int.TryParse(Console.ReadLine(), out int limit);

        Console.Clear();
        Console.WriteLine("Please enter the offset:");

        int.TryParse(Console.ReadLine(), out int offset);

        Console.Clear();
        Console.WriteLine($"All {limit} Customers from index {offset + 1}");

        PrintCustomers(repository.GetPage(offset, limit));
    }
    catch (Exception)
    {

        throw;
    }
    
}

//Read customers by countries
static void TestSelectCountries(ICustomerCountryRepository repository)
{
    Console.Clear();

    foreach (var customerCountry in repository.GetCustomersByCountry())
    {
        Console.WriteLine($"{customerCountry.Country} has {customerCountry.Customers} customers");
    };
}

//Read highest spenders
static void TestSelectHighestSpenders(ICustomerSpenderRepository repository)
{
    Console.Clear();

    foreach (var item in repository.GetHighestSpenders())
    {
        Console.WriteLine($"{item.Name} (with ID: {item.CustomerID}) has spent ${item.InvoiceTotal} in total.");
    }
}

//Read favourite genre by customer id
static void TestSelectFavouriteGenre(ICustomerGenreRepository repository)
{
    Console.Clear();
    Console.WriteLine("Please enter the id of the customer you want to get favourite genre for");

    int.TryParse(Console.ReadLine(), out int id);
    CustomerGenre c = repository.GetFavoriteGenre(id);
    Console.Clear();

    if (c.FavoriteGenre.Count() == 2)
    {
        Console.WriteLine($"ID: {c.Customer.CustomerID}. {(c.Customer.FirstName.EndsWith('s') ? c.Customer.FirstName : c.Customer.FirstName + 's')} favourite genres are {c.FavoriteGenre[0]} and {c.FavoriteGenre[1]} \n");
    }
    else
    {
        Console.WriteLine($"ID: {c.Customer.CustomerID}. {(c.Customer.FirstName.EndsWith('s') ? c.Customer.FirstName : c.Customer.FirstName + 's')} favourite genre is {c.FavoriteGenre[0]} \n");
    }
   
}

//Create
static void TestInsert(ICustomerRepository repository)
{
    Console.Clear();
    Console.WriteLine("Add New Customer: \n");
    Console.WriteLine("Enter first name");
    string first = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Add New Customer: \n");
    Console.WriteLine("Enter last name");
    string last = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Add New Customer: \n");
    Console.WriteLine("Enter country");
    string country = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Add New Customer: \n");
    Console.WriteLine("Enter postal code");
    string postal = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Add New Customer: \n");
    Console.WriteLine("Enter phone number");
    string phone = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Add New Customer: \n");
    Console.WriteLine("Enter E-mail");
    string mail = Console.ReadLine();

    Customer customer = new()
    {
        FirstName = first,
        LastName = last,
        Country = country,
        PostalCode = postal,
        Phone = phone,
        Email = mail
    };

    Console.Clear();

    if (repository.Add(customer))
    {
        Console.WriteLine(customer.FirstName + " was added to the database.");
    }
    else
    {
        Console.WriteLine("Could not create");
    }
}

//Update
static void TestUpdate(ICustomerRepository repository)
{
    Console.Clear();
    Console.WriteLine("Enter the id of the customer you want to edit: \n");
    int.TryParse(Console.ReadLine(), out int id);

    Customer editedCustomer = repository.GetCustomer(id);

    Console.Clear();
    Console.WriteLine("Edit Customer: \n");
    Console.WriteLine("Enter updated first name (or hit Enter to dismiss)");
    string first = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Edit Customer: \n");
    Console.WriteLine("Enter updated last name (or hit Enter to dismiss)");
    string last = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Edit Customer: \n");
    Console.WriteLine("Enter updated country (or hit Enter to dismiss)");
    string country = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Edit Customer: \n");
    Console.WriteLine("Enter updated postal code (or hit Enter to dismiss)");
    string postal = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Edit Customer: \n");
    Console.WriteLine("Enter updated phone number (or hit Enter to dismiss)");
    string phone = Console.ReadLine();

    Console.Clear();
    Console.WriteLine("Edit Customer: \n");
    Console.WriteLine("Enter updated E-mail (or hit Enter to dismiss)");
    string mail = Console.ReadLine();

    if (first != "") editedCustomer.FirstName = first;
    if (last != "") editedCustomer.LastName = last;
    if (country != "") editedCustomer.Country = country;
    if (postal != "") editedCustomer.PostalCode = postal;
    if (phone != "") editedCustomer.Phone = phone;
    if (mail != "") editedCustomer.Email = mail;
    
    if (repository.Update(editedCustomer))
    {
        Console.WriteLine(editedCustomer.FirstName + " was updated.");
    }
    else
    {
        Console.WriteLine("Could not update " + editedCustomer.FirstName);
    }
}

//Delete
static void TestDelete(ICustomerRepository repository)
{
    Console.Clear();
    Console.WriteLine("Delete Customer, please enter the id: \n");
    int.TryParse(Console.ReadLine(), out int id);

    Console.Clear();
    Console.WriteLine("IF YOU ARE SURE YOU WANT TO DELETE THIS RECORD, PRESS 'y'. THIS CANNOT BE UNDONE.");

    ConsoleKeyInfo input = Console.ReadKey();
    if (repository.Delete(id) && input.KeyChar == 'y')
    {
        Console.WriteLine("Deleted...");
    }
    else
    {
        Console.WriteLine("Nothing happened...");
    }
}

//Print a list of customers
static void PrintCustomers(IEnumerable<Customer> customers)
{
    foreach (Customer customer in customers)
    {
        Printcustomer(customer);
    }
}

//Print single customer
static void Printcustomer(Customer customer)
{
    Console.WriteLine($"Customer: --{customer.CustomerID}-- \n Name: {customer.FirstName} {customer.LastName} \n Country: {customer.Country} \n Postal Code: {customer.PostalCode} \n Phone: {customer.Phone}\n E-mail: {customer.Email} \n");
}

