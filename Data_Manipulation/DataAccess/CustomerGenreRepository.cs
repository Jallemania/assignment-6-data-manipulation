﻿using Data_Manipulation.DataAccess.Interfaces;
using Data_Manipulation.Models;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data_Manipulation.DataAccess
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        public CustomerGenre GetFavoriteGenre(int id)
        {
            CustomerRepository customerRepo = new CustomerRepository();
            CustomerGenre customerGenre = new();
            customerGenre.Customer = customerRepo.GetCustomer(id);
            string sql = "SELECT CONCAT(c.FirstName, ' ', c.LastName) AS 'Name', g.Name AS 'Genre', COUNT(g.Name) AS 'Purchases' FROM Customer c"
                + " INNER JOIN Invoice iv ON c.CustomerId = iv.CustomerId"
                + " INNER JOIN InvoiceLine il ON iv.InvoiceId = il.InvoiceId"
                + " INNER JOIN Track t ON il.TrackId = t.TrackId"
                + " INNER JOIN Genre g ON t.GenreId = g.GenreId"
                + $" WHERE c.CustomerId = {id}"
                + " GROUP BY g.Name, c.FirstName, c.LastName"
                + " ORDER BY COUNT(g.Name) DESC"
                + " OFFSET 0 ROWS FETCH NEXT 2 ROWS ONLY";

            try
            {
                //Connect
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();

                    //Command
                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        //Reader
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            //Handle
                            while (reader.Read())
                            {
                                string favoriteGenre = reader.GetString(1);
                                int purchasesGenre = reader.GetInt32(2);

                                if (purchasesGenre >= customerGenre.PurchasesGenre)
                                {
                                    customerGenre.FavoriteGenre.Add(favoriteGenre);
                                    customerGenre.PurchasesGenre = purchasesGenre;
                                }
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }

            return customerGenre;
        }
    }
}
